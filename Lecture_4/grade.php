<?php
    echo "<pre>";
    print_r($_POST);
    echo "</pre>";
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Grade</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <div class="container">
        <h2><?=$_POST['name']." ".$_POST['lastname']?></h2>
        <form action="result.php" method="post">
            <table class="tb-question">
                <thead>
                    <tr>
                        <th>Question</th>
                        <th>Answer</th>
                        <th>Grade</th>
                        <th>Point</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        for($i=0; $i<count($_POST['questions']); $i++){
                    ?>
                    <tr>
                        <td><?=$_POST['questions'][$i]?>
                             <input type="hidden" name="questions[]" value="<?=$_POST['questions'][$i]?>">
                        </td>
                        <td><?=$_POST['answers'][$i]?>
                            <input type="hidden" name="answers[]" value="<?=$_POST['answers'][$i]?> ">
                        </td>
                        <td><input type="number" name="grade[]"></td>
                        <td><?=$_POST['points'][$i]?></td>
                    </tr>
                    <?php
                        }
                    ?>
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan=4>
                            <button>Finish</button>
                        </td>
                    </tr>
                </tfoot>
            </table>
        </form>
    </div>
</body>
</html>