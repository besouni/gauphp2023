<?php
    include "array_of_questions.php";
    shuffle($questions);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Questions</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <div class="container">
        <h2>Questions</h2>
        <form action="grade.php" method="post">
            <table class="tb-question">
                <thead>
                    <tr>
                        <th>Question</th>
                        <th>Answer</th>
                        <th>Point</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        for($i=0; $i<count($questions); $i++){
                    ?>
                    <tr>
                        <td><?=$questions[$i]['question']?>
                            <input type="hidden" name="questions[]" value="<?=$questions[$i]['question']?>">
                        </td>
                        <td><textarea name="answers[]" cols="30" rows="5"></textarea></td>
                        <td><?=$questions[$i]['point']?>
                            <input type="hidden" name="points[]" value="<?=$questions[$i]['point']?>">
                        </td>
                    </tr>
                    <?php
                        }
                    ?>
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan=2>
                            Name: <input type="text" name="name">
                            Lastname: <input type="text" name="lastname">
                        </td>
                        <td>
                            <button>Send</button>
                        </td>
                    </tr>
                </tfoot>
            </table>
        </form>
    </div>
</body>
</html>