<?php
    include "create_file.php";
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <div class="container">
        <nav>
            <ul>
                <li><a href="page.php">Home</a></li>
                <li><a href="?file=create">Create File</a></li>
            </ul>
            <div class="action">
                <?php
                    if(isset($_GET['action']) && $_GET['action']=="write"){
                        include "write_to_file.php";   
                    }elseif(isset($_GET['action']) && $_GET['action']=="read"){
                        include "read_from_file.php";   
                    }
                ?>
            </div>
        </nav>
        <div class="content">
            <h3>Content of Files</h3>
            <table class="table-of-list">
            <?php
                $dir = "files";
                for($i=2; $i<count(scandir($dir)); $i++){
            ?>
                <tr>
                    <td><?=scandir($dir)[$i]?></td>
                    <td><a href="?action=write&&patch=<?=$dir."/".scandir($dir)[$i]?>">Write</a></td>
                    <td><a href="?action=read&&patch=<?=$dir."/".scandir($dir)[$i]?>">Read</a></td>
                </tr>
            <?php } ?>
        </div>
    </div>
   
</body>
</html>