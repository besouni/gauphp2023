<?php
    echo "<pre>";
    // print_r($_SERVER);
    $parsed = parse_url($_SERVER["REQUEST_URI"]);
    // print_r($parsed);
    $filename = basename($parsed['path']);
    // echo $filename;
   
    $active_menu = [
         'index' => "",
         'posts' => "",  
    ];
    // echo $filename;
    if($filename == "index.php"){
        // echo $filename;
        $active_menu['index'] = "active-menu";
    }elseif($filename == "posts.php"){
        $active_menu['posts'] = "active-menu";
    }
    // print_r($active_menu);
    echo "</pre>";
?>
<nav class="navbar-default navbar-side" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav" id="main-menu">
            <li class="text-center"> 
                <img src="assets/img/find_user.png" class="user-image img-responsive"/>
            </li>
            <li>
                <a class="<?=$active_menu['index']?>" href="index.php"><i class="fa fa-dashboard fa-3x"></i> Dashboard</a>
            </li>
                
            <li>
                <a class="<?=$active_menu['posts']?>" href="posts.php"><i class="fa fa-square-o fa-3x"></i>Posts</a>
            </li>	
        </ul>
        
    </div>
</nav> 