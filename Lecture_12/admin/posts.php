﻿<?php
    require_once "connection.php";
    $post_query = "SELECT categorys.title as category_title, CONCAT(users.name,  ' ', COALESCE(users.last_name, '')) as full_name, posts.title, posts.created_at, posts.views 
                   FROM posts
                   INNER JOIN categorys ON categorys.id=posts.category_id
                   INNER JOIN users ON users.id=posts.user_id";
    $post_result = mysqli_query($conn, $post_query);

    $category_query = "SELECT * FROM Categorys";
    $category_result = mysqli_query($conn, $category_query);

    $user_query = "SELECT * FROM users";
    $user_result = mysqli_query($conn, $user_query);

    if(isset($_POST['save'])){
        $category_id = $_POST['category_id'];
        $user_id = $_POST['user_id'];
        $title = $_POST['title'];
        $content = $_POST['content'];
        $post_insert = "INSERT INTO posts(category_id, user_id, title, content) VALUES ('$category_id', '$user_id', '$title', '$content')";
        mysqli_query($conn, $post_insert);
        header("location: posts.php");
    }
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Binary Admin</title>
	<!-- BOOTSTRAP STYLES-->
    <link href="assets/css/bootstrap.css" rel="stylesheet" />
     <!-- FONTAWESOME STYLES-->
    <link href="assets/css/font-awesome.css" rel="stylesheet" />
        <!-- CUSTOM STYLES-->
    <link href="assets/css/custom.css" rel="stylesheet" />
     <!-- GOOGLE FONTS-->
   <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
</head>
<body>
    <div id="wrapper">
        <?php 
            include_once "blocks/top_nav.php"; 
            include_once "blocks/nav.php";         
        ?>
        <!-- /. NAV SIDE  -->
        <div id="page-wrapper" >
            <div id="page-inner">
                <div class="row">
                    <div class="panel panel-default">
                            <div class="panel-body">
                            <button class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal">
                              + Add new post
                            </button>
                            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content" style="min-width:100%">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                            <h4 class="modal-title" id="myModalLabel">Add Post</h4>
                                        </div>
                                        <form action="" role="form" method="post">
                                            <div class="modal-body">
                                                <div class="form-group">
                                                    <label>Categories</label>
                                                    <select class="form-control" name="category_id"/>
                                                        <?php
                                                            while($category_row = mysqli_fetch_assoc($category_result)){
                                                        ?>
                                                        <option value="<?=$category_row['id']?>"><?=$category_row['title']?></option>
                                                        <?php
                                                            }
                                                        ?>
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label>Users</label>
                                                    <select class="form-control" name="user_id"/>
                                                        <?php
                                                            while($user_row = mysqli_fetch_assoc($user_result)){
                                                        ?>
                                                        <option value="<?=$user_row['id']?>"><?=$user_row['name']." ".$user_row['last_name']?></option>
                                                        <?php
                                                            }
                                                        ?>
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label>Title</label>
                                                    <input class="form-control" name="title" />
                                                </div>
                                                <div class="form-group">
                                                    <label>Content</label>
                                                    <textarea class="form-control" name="content" rows="5"></textarea>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                <input class="btn btn-primary" type="submit" name="save" value="SAVE">
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                 <!-- /. ROW  -->
                 <hr />
                 <div class="row">
                <div class="col-md-12">
                    <!-- Advanced Tables -->
                    <div class="panel panel-default">
                        <div class="panel-heading">
                             Advanced Tables
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
                                            <th>Category</th>
                                            <th>Author</th>
                                            <th>Title</th>
                                            <th>Date</th>
                                            <th>View</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                            while($post_row = mysqli_fetch_assoc($post_result)){
                                        ?>
                                        <tr class="odd gradeX">
                                            <td><?=$post_row['category_title']?></td>
                                            <td><?=$post_row['full_name']?></td>
                                            <td><?=$post_row['title']?></td>
                                            <td><?=$post_row['created_at']?></td>
                                            <td><?=$post_row['views']?></td>
                                        </tr>
                                        <?php
                                            }
                                        ?>
                                    </tbody>
                                </table>
                            </div> 
                        </div>
                    </div>
                    <!--End Advanced Tables -->
                </div>
            </div>
            </div>
             <!-- /. PAGE INNER  -->
        </div>
         <!-- /. PAGE WRAPPER  -->
    </div>
    <!-- /. WRAPPER  -->
    <!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
    <!-- JQUERY SCRIPTS -->
    <script src="assets/js/jquery-1.10.2.js"></script>
      <!-- BOOTSTRAP SCRIPTS -->
    <script src="assets/js/bootstrap.min.js"></script>
    <!-- METISMENU SCRIPTS -->
    <script src="assets/js/jquery.metisMenu.js"></script>
    <!-- DATA TABLE SCRIPTS -->
    <script src="assets/js/dataTables/jquery.dataTables.js"></script>
    <script src="assets/js/dataTables/dataTables.bootstrap.js"></script>
        <script>
            $(document).ready(function () {
                $('#dataTables-example').dataTable();
            });
        </script>
    <!-- CUSTOM SCRIPTS -->
    <script src="assets/js/custom.js"></script>  
</body>
</html>
