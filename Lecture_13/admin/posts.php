<?php
    include_once "../config/connection.php";
    $post_query = "SELECT categorys.title as category_title, CONCAT(users.name,  ' ', COALESCE(users.last_name, '')) as full_name, posts.title, posts.created_at, posts.views 
                   FROM posts
                   INNER JOIN categorys ON categorys.id=posts.category_id
                   INNER JOIN users ON users.id=posts.user_id";
    $post_result = mysqli_query($conn, $post_query);

    $category_query = "SELECT * FROM Categorys";
    $category_result = mysqli_query($conn, $category_query);

    $user_query = "SELECT * FROM users";
    $user_result = mysqli_query($conn, $user_query);

    if(isset($_POST['save'])){
        $category_id = $_POST['category_id'];
        $user_id = $_POST['user_id'];
        $title = $_POST['title'];
        $content = $_POST['content'];
        $post_insert = "INSERT INTO posts(category_id, user_id, title, content) VALUES ('$category_id', '$user_id', '$title', '$content')";
        mysqli_query($conn, $post_insert);
        header("location: posts.php");
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Posts</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.7.0/jquery.min.js"></script>
    <link rel="stylesheet" href="https://cdn.datatables.net/1.13.4/css/jquery.dataTables.css" />
    <script src="https://cdn.datatables.net/1.13.4/js/jquery.dataTables.js"></script>
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <header></header>
    <main>
        <?php
            include_once "blocks/nav.php";
        ?>
        <aside>
            <div>
                <form action="" method="post">
                    <label for="">Categories</label>
                    <select name="category_id">
                        <?php
                            while($category_row = mysqli_fetch_assoc($category_result)){
                        ?>
                        <option value="<?=$category_row['id']?>"><?=$category_row['title']?></option>
                        <?php
                            }
                        ?>
                    </select>
                    <br><br>
                    <label for="">Users</label>
                    <select name="user_id">
                         <?php
                            while($user_row = mysqli_fetch_assoc($user_result)){
                        ?>
                        <option value="<?=$user_row['id']?>"><?=$user_row['name']." ".$user_row['last_name']?></option>
                        <?php
                            }
                        ?>
                    </select>
                    <br><br>
                    <label for="">Title</label>
                    <input type="text" name="title">
                    <br><br>
                    <label for="">Content</label>
                    <textarea name="content" id="" cols="30" rows="10"></textarea>
                    <br><br>
                    <button name="save">+Add New Post</button>
                </form>
            </div>
            <br><br>
            <table id="myTable" class="display">
                <thead>
                    <tr>
                        <th>Category</th>
                        <th>Author</th>
                        <th>Title</th>
                        <th>Date</th>
                        <th>View</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        while($post_row = mysqli_fetch_assoc($post_result)){
                    ?>
                    <tr class="odd gradeX">
                        <td><?=$post_row['category_title']?></td>
                        <td><?=$post_row['full_name']?></td>
                        <td><?=$post_row['title']?></td>
                        <td><?=$post_row['created_at']?></td>
                        <td><?=$post_row['views']?></td>
                    </tr>
                    <?php
                        }
                    ?>
                </tbody>
            </table>
            <script>
                $(document).ready( function () {
                    $('#myTable').DataTable();
                } );
            </script>
        </aside>
    </main>
    <footer></footer>
</body>
</html>