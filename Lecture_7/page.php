<?php
// ატვირთეთ მხოლოდ .txt, .jpg, .png გაფართოების ფაილები.
// ატვირთეთ მხოლოდ 20 მბ-ზე ნაკლები ფაილები.
// ატვირთვის დროს არ მოხდეს ფაილების ერთმანეთზე გადაწერა
    include "uploaded.php";
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Files And Folders</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <div class="container">
        <div class="upload">
             <h3>Upload file inside root</h3>
             <form method="post" enctype="multipart/form-data">
                <input type="file" name="f_name">
                <br><br>
                <button name="f_upload">Upload</button>
             </form>
        </div>
        <div class="root">
            <h3>Content of root</h3>
            <table style="width: 100%;">
            <?php
                for($i=2; $i<count(scandir("root")); $i++){
            ?>
                <tr>
                    <td><a href="<?="root/".scandir("root")[$i]?>"  download=""><?=scandir("root")[$i]?></a></td>
                    <td><a href="?drop=<?="root/".scandir("root")[$i]?>">DROP</a></td>
                </tr>
            <?php } ?>
            </table>
        </div>
    </div>
</body>
</html>