<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Lecture 5</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <?php
        // include "functions.php";
        include "files.php";
    ?>
    <div class="container">
        <h2>Create Folder</h2>
        <p><a href="show.php">Folders</a></p>
        <form action="files.php" method="post">
            <input type="text" placeholder="folder name" name="folder_name">
            <br><br>
            <button name="c_folder">create folder</button>
        </form>
    </div>
</body>
</html>