<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Home</title>
</head>
<body>
    <ul>
        <li><a href="web1.php">WEB 1</a></li>
        <li><a href="web2.php">WEB 2</a></li>
        <li><a href="?url=3">WEB 3</a></li>
        <li><a href="?url=4">WEB 4</a></li>
    </ul>
    <?php
        if(isset($_GET["url"]) && $_GET["url"]==3){
            echo "<h1>WEB 3</h1>";
        }else if(isset($_GET["url"]) && $_GET["url"]==4){
            echo "<h1>WEB 4</h1>";
        }
    ?>
</body>
</html>