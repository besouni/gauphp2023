<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PHP - 2</title>
</head>
<body>
    <?php
        $a2 = ["name"=>"Temo", "age"=>19, "university"=>"GAU", "grades"=>[90, 78, 56, 23]];
        echo "<pre>"; 
        print_r($a2);
        echo "</pre>";
        echo "<hr>";
        echo "<h1>PHP</h1>";  
        $x = "34.8";
        var_dump($x);
        echo "<hr>";
        $a1 = [34, 5, 8, 7.9, "HTML", "CSS", 9.76];
        $a1[10] = 89;
        // echo $a1;
        echo "<pre>"; 
        var_dump($a1);
        echo "</pre>"; 
        echo "<hr>";
        echo "<pre>"; 
        print_r($a1);
        echo "</pre>"; 
    ?>
</body>
</html>